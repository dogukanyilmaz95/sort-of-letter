package com.dogukan.sortofletter;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Scanner;


public class App {
    public static void main(String[] args) {
        System.out.println("You will write 11 word for start!");
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> words = new ArrayList<>();

        for (int i = 0; i < 11; i++) {
            words.add(scanner.next());
        }
        SortsOfWord(words);
    }

    public static String SortsOfWord(ArrayList words) {
        ArrayList<String> arrayList1 = new ArrayList<String>();
        ArrayList<String> arrayList2 = new ArrayList<String>();
        String word;
        for (int i = 0; i < words.size(); i++) {
            word = words.get(i).toString();
            if (word.contains("a")) {
                arrayList1.add(word);
            } else {
                arrayList2.add(word);
            }
        }
        String[] words1 = new String[arrayList1.size()];
        String[] words2 = new String[arrayList2.size()];
        for (int j = 0; j < arrayList1.size(); j++) {
            words1[j] = arrayList1.get(j).toString();
        }
        for (int j = 0; j < arrayList2.size(); j++) {
            words2[j] = arrayList2.get(j).toString();
        }

        String ctrl;
        int counter2 = 0;
        int counter1 = 0;

        String character;
        String har;
        for (int i = 0; i < words1.length; i++) {
            character = words1[i].toString();
            counter1 = 0;
            for (int j = 0; j < character.length(); j++) {
                if (character.substring(j, j + 1).equals("a")) {
                    counter1++;
                }
            }
            for (int k = 0; k < words1.length; k++) {
                character = words1[k].toString();
                counter2 = 0;
                for (int j = 0; j < character.length(); j++) {
                    if (character.substring(j, j + 1).equals("a")) {
                        counter2++;
                    }
                }
                if (counter2 < counter1) {
                    ctrl = words1[i];
                    words1[i] = words1[k];
                    words1[k] = ctrl;
                }

            }
        }

        for (int i = 0; i < words2.length; i++) {
            for (int k = 0; k < words2.length; k++) {

                if (words2[k].length() < words2[i].length()) {
                    ctrl = words2[i];
                    words2[i] = words2[k];
                    words2[k] = ctrl;
                }

            }
        }
        String result = "";
        for (int i = 0; i < words1.length; i++) {
            System.out.print(words1[i] + ", ");
            result += words1[i] + ", ";
        }
        for (int i = 0; i < words2.length; i++) {
            System.out.print(words2[i] + ", ");
            result += words2[i] + ", ";
        }
        return result;
    }
}

