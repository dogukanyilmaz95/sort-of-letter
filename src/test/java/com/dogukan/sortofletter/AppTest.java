package com.dogukan.sortofletter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.ArrayList;

/**
 * Unit test for simple App.
 */
public class AppTest {
    @Test
    public void SortOfLetterTest() {
        ArrayList<String> words = new ArrayList<>();
        words.add("aaaasd");
        words.add("a");
        words.add("aab");
        words.add("aaabcd");
        words.add("ef");
        words.add("cssssssd");
        words.add("fdz");
        words.add("kf");
        words.add("zc");
        words.add("lklklklklklklklkl");
        words.add("l");

        App app = new App();

        String result = app.SortsOfWord(words);

        assertEquals("aaaasd, aaabcd, aab, a, lklklklklklklklkl, cssssssd, fdz, zc, kf, ef, l, ", result);

    }
}
